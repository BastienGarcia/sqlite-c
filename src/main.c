#include <stdio.h>
#include "exec_request.h"

int main(void)
{
    sqlite3 *db;
    if (sqlite3_open("./database.db", &db) != SQLITE_OK) {
        printf("Failed to open database");
        return (84);
    }
    const char *request =
            "DROP TABLE IF EXISTS Results;"
            "CREATE TABLE Results(Id INTEGER PRIMARY KEY, Date TEXT, RGB INT, PatientName TEXT, TestName TEXT);"
            "INSERT INTO Results VALUES(1, '08/05/20', 0, 'Foo', 'Test A');"
            "INSERT INTO Results VALUES(2, '08/05/20', 0, 'Bar', 'Test A');"
            "INSERT INTO Results VALUES(3, '08/05/20', 0, 'Toto', 'Test A');"
            "INSERT INTO Results VALUES(4, '08/05/20', 0, 'Foo', 'Test B');"
            "INSERT INTO Results VALUES(5, '08/05/20', 0, 'Foo', 'Test C');";
    printf("Request status: %d\n", exec_request(db, request));
    sqlite3_close(db);
    printf("Database closed\n");
    return 0;
}