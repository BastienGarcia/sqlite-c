#include <stdio.h>
#include "sqlite3.h"

int exec_request(sqlite3 *database, const char *request)
{
    printf("%s\n", request);
    return sqlite3_exec(database, request, 0, 0, 0);
}