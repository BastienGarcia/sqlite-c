#include "sqlite3.h"

#ifndef EXEC_REQUEST
#define EXEC_REQUEST

int exec_request(sqlite3 *database, const char *request);

#endif // EXEC_REQUEST